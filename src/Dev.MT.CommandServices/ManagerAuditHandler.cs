﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MassTransit;
using Dev.MT.Commands;
using Dev.MT.Domain.Models;
using Dev.MT.Events;
using Dev.MT.Repository;

namespace Dev.MT.CommandServices
{
    public class ManagerAuditHandler :
        IConsumer<ApproveLeaveByManager>,
        IConsumer<DeclineLeaveByManager>
    {
        protected DevMtDbContext DbContext { get; private set; }

        public ManagerAuditHandler(DevMtDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public async Task Consume(ConsumeContext<ApproveLeaveByManager> context)
        {
            var command = context.Message;

            var leave = await DbContext.Leaves.FindAsync(command.LeaveId);

            if (leave == null)
                throw new ArgumentException($"Cound not find leave with the id [{command.LeaveId}] that was provided.");

            var @event = new LeaveApprovedByManager(leave.Id, leave.JobGrade);
            await context.Publish(@event);

            await context.RespondAsync(new ApproveLeaveByManagerResponse(leave.Id));
        }

        public async Task Consume(ConsumeContext<DeclineLeaveByManager> context)
        {
            var command = context.Message;

            var leave = await DbContext.Leaves.FindAsync(command.LeaveId);

            if (leave == null)
                throw new ArgumentException($"Cound not find leave with the id [{command.LeaveId}] that was provided.");

            var @event = new LeaveDeclinedByManager(leave.Id, leave.JobGrade);
            await context.Publish(@event);

            await context.RespondAsync(new DeclineLeaveByManagerResponse(leave.Id));
        }
    }
}
