﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MassTransit;
using Dev.MT.Commands;
using Dev.MT.Domain.Models;
using Dev.MT.Events;
using Dev.MT.Repository;
using MassTransit.Logging;

namespace Dev.MT.CommandServices
{
    public class NotifyLeaderAuditLeaveHandler : IConsumer<NotifyLeaderAuditLeave>
    {
        protected log4net.ILog Log { get; private set; }
        protected DevMtDbContext DbContext { get; private set; }

        public NotifyLeaderAuditLeaveHandler(log4net.ILog log, DevMtDbContext dbContext)
        {
            Log = log;
            DbContext = dbContext;
        }

        public async Task Consume(ConsumeContext<NotifyLeaderAuditLeave> context)
        {
            var command = context.Message;

            var leave = await DbContext.Leaves.FindAsync(command.LeaveId);

            if (leave == null)
                throw new ArgumentException($"Cound not find leave with the id [{command.LeaveId}] that was provided.");

            Log.Info($"Notify Leader [LeaveId = {leave.Id}]");
        }
    }
}
