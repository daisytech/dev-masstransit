﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Dev.MT.ApplicationServices;
using Dev.MT.ViewModels;

namespace Dev.MT.Web.Controllers
{
    [RoutePrefix("Leave")]
    public class LeaveController : Controller
    {
        protected readonly LeaveService LeaveService;

        public LeaveController(LeaveService leaveService)
        {
            LeaveService = leaveService;
        }

        [Route("")]
        public async Task<ActionResult> Index()
        {
            var vm = await LeaveService.CreateLeaveOverviewViewModelAsync();

            return View(vm);
        }

        [Route("Create")]
        public ActionResult Create()
        {
            var vm = LeaveService.CreateCreateLeaveViewModel();

            return View(vm);
        }

        [Route("Create")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateLeaveViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var leaveId = await LeaveService.CreateLeaveAsync(vm);
                if (leaveId > 0)
                    return RedirectToAction("Details", new { leaveId });
            }

            return View(vm);
        }

        [Route("{leaveId:int}/Details")]
        public async Task<ActionResult> Details(int leaveId)
        {
            var vm = await LeaveService.CreateLeaveDetailsViewModelAsync(leaveId);

            return View(vm);
        }

        [Route("{leaveId:int}/Submit")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Submit(int leaveId)
        {
            await LeaveService.SubmitLeaveAsync(leaveId);

            return RedirectToAction("Details", new { leaveId });
        }

        [Route("{leaveId:int}/Cancel")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Cancel(int leaveId)
        {
            await LeaveService.CancelLeaveAsync(leaveId);

            return RedirectToAction("Details", new { leaveId });
        }

        [Route("{leaveId:int}/Leader/Audit")]
        public async Task<ActionResult> AuditByLeader(int leaveId)
        {
            var vm = await LeaveService.CreateLeaveDetailsViewModelAsync(leaveId);
            
            return View("AuditByLeader", vm);
        }

        [Route("{leaveId:int}/Leader/Approve")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ApproveByLeader(int leaveId)
        {
            await LeaveService.ApproveByLeaderAsync(leaveId);

            return RedirectToAction("AuditByLeader", new { leaveId });
        }

        [Route("{leaveId:int}/Leader/Decline")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeclineByLeader(int leaveId)
        {
            await LeaveService.DeclineByLeaderAsync(leaveId);

            return RedirectToAction("AuditByLeader", new { leaveId });
        }

        [Route("{leaveId:int}/Manager/Audit")]
        public async Task<ActionResult> AuditByManager(int leaveId)
        {
            var vm = await LeaveService.CreateLeaveDetailsViewModelAsync(leaveId);

            return View("AuditByManager", vm);
        }

        [Route("{leaveId:int}/Manager/Approve")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ApproveByManager(int leaveId)
        {
            await LeaveService.ApproveByManagerAsync(leaveId);

            return RedirectToAction("AuditByManager", new { leaveId });
        }

        [Route("{leaveId:int}/Manager/Decline")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeclineByManager(int leaveId)
        {
            await LeaveService.DeclineByManagerAsync(leaveId);

            return RedirectToAction("AuditByManager", new { leaveId });
        }
    }
}