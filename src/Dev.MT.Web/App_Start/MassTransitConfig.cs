﻿
using System;
using System.Diagnostics;
using Dev.MT.Common;
using Dev.MT.Saga;
using MassTransit;
using MassTransit.Log4NetIntegration;
using MassTransit.Saga;
using Microsoft.Practices.Unity;
using Automatonymous;
using Dev.MT.Saga.LeaveProcess;
using MassTransit.EntityFrameworkIntegration.Saga;
using MassTransit.UnityIntegration;

namespace Dev.MT.Web.App_Start
{
    public class MassTransitConfig
    {
        public static void Register(IUnityContainer container)
        {
            var bus = BuildBus(container);

            container.RegisterInstance<IBus>(bus, new ContainerControlledLifetimeManager());
        }

        private static IBus BuildBus(IUnityContainer container)
        {
            var requestSetttings = new RequestSettingsImpl()
            {
                ServiceAddress = new Uri("loopback://localhost/input_queue"),
                SchedulingServiceAddress = new Uri("loopback://localhost/quartz"),
                Timeout = Debugger.IsAttached ? TimeSpan.FromMinutes(5) : TimeSpan.FromSeconds(30)
            };

            container.RegisterInstance<ISagaRepository<LeaveProcessState>>(
                new EntityFrameworkSagaRepository<LeaveProcessState>(() => new LeaveProcessDbContext()),
                new ContainerControlledLifetimeManager());

            var bus = Bus.Factory.CreateUsingInMemory(cfg =>
            {
                cfg.UseLog4Net();

                cfg.ReceiveEndpoint("input_queue", ep =>
                {
                    ep.LoadFrom(container);
                });

                cfg.ReceiveEndpoint("leaveprocess_saga_queue", ep =>
                {
                    ep.UseConcurrencyLimit(1);
                    ep.StateMachineSaga(new LeaveProcessStateMachine(requestSetttings),
                        new UnitySagaRepository<LeaveProcessState>(
                            container.Resolve<ISagaRepository<LeaveProcessState>>(), container));
                });
            });

            var handle = bus.Start();

            return bus;
        }
    }
}