namespace Dev.MT.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLeaveActivityTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dev.LeaveActivities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Description = c.String(maxLength: 255),
                        LeaveId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dev.Leaves", t => t.LeaveId, cascadeDelete: true)
                .Index(t => t.LeaveId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dev.LeaveActivities", "LeaveId", "dev.Leaves");
            DropIndex("dev.LeaveActivities", new[] { "LeaveId" });
            DropTable("dev.LeaveActivities");
        }
    }
}
