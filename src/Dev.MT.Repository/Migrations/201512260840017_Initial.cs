namespace Dev.MT.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dev.Leaves",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        JobGrade = c.Int(nullable: false),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        Hours = c.Int(nullable: false),
                        Note = c.String(maxLength: 255),
                        State = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dev.Leaves");
        }
    }
}
