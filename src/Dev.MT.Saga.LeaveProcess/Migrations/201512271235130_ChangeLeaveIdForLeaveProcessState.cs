namespace Dev.MT.Saga.LeaveProcess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeLeaveIdForLeaveProcessState : DbMigration
    {
        public override void Up()
        {
            AlterColumn("state.LeaveProcessStates", "LeaveId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("state.LeaveProcessStates", "LeaveId", c => c.Int(nullable: false));
        }
    }
}
