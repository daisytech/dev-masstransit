namespace Dev.MT.Saga.LeaveProcess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "state.LeaveProcessStates",
                c => new
                    {
                        CorrelationId = c.Guid(nullable: false),
                        CurrentState = c.String(maxLength: 64),
                        LeaveId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CorrelationId);
            
        }
        
        public override void Down()
        {
            DropTable("state.LeaveProcessStates");
        }
    }
}
