﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dev.MT.ViewModels
{
    public class LeaveDetailsViewModel
    {
        public int LeaveId { get; set; }
        public int JobGrade { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int Hours { get; set; }
        public string Note { get; set; }
        public string State { get; set; }
        public IList<LeaveActivityDetailsViewModel> Activities { get; private set; }

        public LeaveDetailsViewModel()
        {
            Activities = new List<LeaveActivityDetailsViewModel>();
        }
    }
}
