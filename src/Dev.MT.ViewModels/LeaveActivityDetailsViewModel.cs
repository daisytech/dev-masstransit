﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dev.MT.ViewModels
{
    public class LeaveActivityDetailsViewModel
    {
        public DateTime Date { get; set; }
        public string Description { get; set; }
    }
}
