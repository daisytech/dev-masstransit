﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dev.MT.Commands
{
    public class SubmitLeave
    {
        public SubmitLeave(int leaveId)
        {
            if(leaveId==default(int)) throw new ArgumentOutOfRangeException(nameof(leaveId));

            LeaveId = leaveId;
        }

        public int LeaveId { get; private set; }
    }

    public class SubmitLeaveResponse
    {
        public SubmitLeaveResponse(int leaveId)
        {
            LeaveId = leaveId;
        }

        public int LeaveId { get; private set; }
    }
}
